Rails.application.routes.draw do
  get 'health_record/new'
  get 'health_record/index'

  
  
  get 'comprobantes/new'
  post 'comprobantes/create'
  
  get 'health_record/new'
  post 'health_record/create'
  
  get 'registrations/edit'

  get 'turnos/index'
  
  get 'users/index'
  
  resources :solicitar_turnos
  resources :comprobantes

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions'}
  root to: "home#index"
end
