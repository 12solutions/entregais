json.extract! comprobante, :id, :vacuna, :fecha, :descripcion, :observaciones, :created_at, :updated_at
json.url comprobante_url(comprobante, format: :json)
