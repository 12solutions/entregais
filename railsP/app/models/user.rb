class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :authentication_keys => [:token,:email]
  has_many :comprobantes, :dependent => :destroy
  has_one :personal_record, :dependent => :destroy
  has_one :health_record, :dependent => :destroy
  has_many :TurnoAsignado, :dependent => :destroy 
  has_many :TurnoNoAsignado, :dependent => :destroy
end
