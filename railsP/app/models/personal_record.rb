class PersonalRecord < ApplicationRecord
    belongs_to :user
    validates :dni, presence: true, uniqueness: true
    validates :nombre, presence: true
    validates :apellido, presence: true
    before_save :upcase_content

    def upcase_content
        self.nombre=self.nombre.downcase
        self.apellido=self.apellido.downcase
        self.nombre=self.nombre.split(/ |\_/).map(&:capitalize).join(" ")
        self.apellido=self.apellido.split(/ |\_/).map(&:capitalize).join(" ")
    end
end
