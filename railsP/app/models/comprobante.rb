class Comprobante < ApplicationRecord
    belongs_to :user
    enum vacuna: {
        covid1:0,
        covid2:1,
        gripe:2,
        fiebre:3
    }
    def humanize
        case self.vacuna
        when 'covid1'
            return "Primera dosis COVID 19"
        when 'covid2'
            return "Segunda dosis COVID 19"
        when 'gripe'
            return "Vacuna antigripal"
        when 'fiebre'
            return "Vacuna contra la fiebre amarilla"
        end
    end
end
