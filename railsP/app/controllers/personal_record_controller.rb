class PersonalRecordController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  
  def new
    @record = PersonalRecord.new
  end

  def create
    @record = PersonalRecord.new(record_params)
    @record.user_id=current_user.id
    if @record.save
      flash[:notice] = "Tus datos personales fueron cargados exitosamente! Por favor, completá tu información de salud"
      redirect_to users_index_path
    else
      render :new
    end
  end

  private
  def record_params
    params.require(:personal_record).permit(:dni, :nombre, :apellido)
  end

end
