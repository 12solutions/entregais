class HealthRecordController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  
  def new
    @record = HealthRecord.new
  end

  def create
    @record = HealthRecord.new(record_params)
    @record.user_id=current_user.id
    @turno = TurnoNoAsignado.new(lugar: "Cementerio", vacuna: "Primer dosis covid", user_id: current_user.id,fecha: DateTime.now.strftime("%d/%m/%Y %H:%M:%S"))
    if @record.save
      flash[:notice] = "Tus datos personales fueron cargados exitosamente! Por favor, carga los datos de tus vacunas previas"
      redirect_to new_comprobante_path
      @turno.save
    else
      render :new
    end
  end

  private
  def record_params
    params.require(:health_record).permit(:dni, :nombre, :apellido,:risk,:birth)
  end

end