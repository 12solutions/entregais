class ComprobantesController < ApplicationController
  before_action :set_comprobante, only: %i[ show edit update destroy ]

  # GET /comprobantes or /comprobantes.json
  def index
    @u = current_user
    @comprobantes = Comprobante.all.where(user_id:@u.id,printable:true)
    #get usuario current role

    respond_to do |format|
      format.html
      format.json
      format.pdf { render template: "comprobantes/comp.pdf.erb", pdf: "Comprobante_#{current_user.health_record.dni}" }
    end
  end

  # GET /comprobantes/1 or /comprobantes/1.json
  def show
    @datos=current_user.health_record
    respond_to do |format|
      format.html
      format.json
      format.pdf{ 
        render pdf: "Comprobante_#{current_user.health_record.dni}",
        template: "comprobantes/comp.pdf.erb"  # Excluding ".pdf" extension.
      }
    end
  end

  # GET /comprobantes/new
  def new
    @comprobante = Comprobante.new
    @u=current_user
    @comprobantes = Comprobante.all.where(user_id:@u.id,printable:false)
  end

  # GET /comprobantes/1/edit
  def edit
  end

  # POST /comprobantes or /comprobantes.json
  def create
    @comprobante = Comprobante.new(comprobante_params)
    @usuario = current_user
    @turno=TurnoNoAsignado.where(:user_id =>@usuario.id)
    if @usuario.role == "Paciente"
      @comprobante.printable = false
      @comprobante.user_id = @usuario.id
    else
      @comprobante.printable = true
    end
    respond_to do |format|
      if !current_user.comprobantes.where(:vacuna =>@comprobante.vacuna).blank?
        format.html{ redirect_to new_comprobante_path, notice: 'Usted ya tiene esta vacuna asignada' }
      elsif @comprobante.vacuna == "covid2" && current_user.comprobantes.where(:vacuna =>0).blank?
        format.html{ redirect_to new_comprobante_path, notice: 'Usted no puede tener la segunda dosis sin antes la primera!!' }
      elsif @comprobante.save
        if @comprobante.vacuna == "covid1"
          @turno.destroy_all
          @turno = TurnoNoAsignado.new(lugar: "Cementerio", vacuna: "Segunda dosis covid", user_id: current_user.id,fecha: DateTime.now.strftime("%d/%m/%Y %H:%M:%S"))
          @turno.save
        elsif @comprobante.vacuna == "covid2"
          @turno.destroy_all
        end
        if current_user.role == "Paciente"
          format.html { redirect_to new_comprobante_path, notice: "Vacuna previa agregada a sus datos" }
        else
          format.html { redirect_to comprobantes_url, notice: "Comprobante was successfully destroyed." }
          format.json { head :no_content }
        end
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @comprobante.errors, status: :unprocessable_entity }
      end

    end
  end

  # PATCH/PUT /comprobantes/1 or /comprobantes/1.json
  def update
    respond_to do |format|
      if @comprobante.update(comprobante_params)
        if current_user.role == "Paciente"
          format.html { redirect_to edit_user_registration_path, notice: "Vacuna editada" }
        else
          format.html { redirect_to comprobantes_url, notice: "Comprobante was successfully destroyed." }
          format.json { head :no_content }
        end
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @comprobante.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comprobantes/1 or /comprobantes/1.json
  def destroy
    @comprobante.destroy
    @usuario = current_user
    @turno=TurnoNoAsignado.where(:user_id =>@usuario.id)
    if @comprobante.vacuna == "covid1"
      @turno.destroy_all
      @turno = TurnoNoAsignado.new(lugar: "Cementerio", vacuna: "Primera dosis covid", user_id: current_user.id,fecha: DateTime.now.strftime("%d/%m/%Y %H:%M:%S"))
      @turno.save
    elsif @comprobante.vacuna == "covid2"
      @turno.destroy_all
      @turno = TurnoNoAsignado.new(lugar: "Cementerio", vacuna: "Segunda dosis covid", user_id: current_user.id,fecha: DateTime.now.strftime("%d/%m/%Y %H:%M:%S"))
      @turno.save
    end
    respond_to do |format|
      if current_user.role == "Paciente"
        format.html { redirect_to new_comprobante_path, notice: "Vacuna borrada" }
      else
        format.html { redirect_to comprobantes_url, notice: "Comprobante was successfully destroyed." }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comprobante
      @comprobante = Comprobante.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def comprobante_params
      params.require(:comprobante).permit(:vacuna, :fecha, :descripcion, :observaciones, :user_id)
    end
end
