class TurnosController < ApplicationController
  
  def index
    @turnoNoAsignado = User.find(current_user.id).TurnoNoAsignado
    @turnoAsignado = User.find(current_user.id).TurnoAsignado
    if(User.find(current_user.id).TurnoNoAsignado.count == 0) && (User.find(current_user.id).TurnoAsignado.count == 0)
      flash.now.notice = "No existen solicitudes de turnos y tampoco turnos asignados"
    else
      if(User.find(current_user.id).TurnoAsignado.count == 0)
        flash.now.notice = "No existen turnos asignados"
      else
          if (User.find(current_user.id).TurnoNoAsignado.count == 0)
            flash.now.notice = "No existen solicitudes de turnos"
          end
      end
    end
  end
end
