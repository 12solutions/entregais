class SolicitarTurnosController < ApplicationController
  skip_before_action :verify_authenticity_token
  def new
    @turno = TurnoNoAsignado.new
  end

  def create
    b= true;
    mensaje= "";
    #Si ya existe un pedido de la vacuna -> chau
    if (!User.find(current_user.id).TurnoNoAsignado.where(:vacuna => params[:vacuna]).blank?) 
        b= false
        mensaje = "Ya existe una solicitud de turno para esta vacuna."
    end

     #Si selecciono gripe                                 / /ya se la aplicó
    if (params[:vacuna] == "Gripe") && (!User.find(current_user.id).comprobantes.where(:vacuna => 2).blank?)
      #diferencia entre las fechas para ver si pasó un año desde la ultima aplicacion
      a = ((DateTime.now - (User.find(current_user.id).comprobantes.find_by(:vacuna => 2).fecha)))
      diferencia = a.to_i

      if((Date.today- User.find(current_user.id).health_record.birth).to_i < 365*60 )
          if (diferencia < 365 - 6*30)
            b= false
            mensaje = "El turno es a 6 meses por lo que debe esperar 6 meses despues de su ultima aplicacion esta vacuna."
          end
      else
          if (diferencia < 365 - 3*30)
            b= false
            mensaje ="El turno es a 3 meses por lo que debe esperar 9 meses despues de su ultima aplicacion de esta vacuna."
          end
      end
      
    end

    #Si el usuario pidió fiebre amarilla y tiene mas de 60 años -> chau
    if((params[:vacuna] == "Fiebre amarilla") && ((Date.today- User.find(current_user.id).health_record.birth).to_i > 365*60 ))
        b= false
        mensaje = "Lo sentimos, pero no aplicamos la vacuna de la fiebre amarilla a mayores de 60 años por posibles riesgos adversos. "
    end

     #si es fiebre amarilla y existe una vacuna aplicada de fiebre amarilla
    if ((params[:vacuna] == "Fiebre amarilla") && (!User.find(current_user.id).comprobantes.where(:vacuna => 3 ).blank?))
      b= false
      mensaje = "Ya tiene aplicada la vacuna de la fiebre amarilla."
    end
    
    if(b)
      @turno = TurnoNoAsignado.new(lugar: params[:lugar], vacuna: params[:vacuna], user_id: current_user.id,fecha: DateTime.now.strftime("%d/%m/%Y %H:%M:%S"))
      if @turno.save
        redirect_to turnos_index_path
      end  
    else
      flash[:notice] = mensaje
      redirect_to new_solicitar_turno_path
    end 
  end
end

