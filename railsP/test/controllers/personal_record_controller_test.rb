require "test_helper"

class PersonalRecordControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get personal_record_new_url
    assert_response :success
  end

  test "should get create" do
    get personal_record_create_url
    assert_response :success
  end
end
