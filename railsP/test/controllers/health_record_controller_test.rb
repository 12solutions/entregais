require "test_helper"

class HealthRecordControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get health_record_new_url
    assert_response :success
  end

  test "should get index" do
    get health_record_index_url
    assert_response :success
  end
end
