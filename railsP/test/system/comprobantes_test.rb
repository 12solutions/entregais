require "application_system_test_case"

class ComprobantesTest < ApplicationSystemTestCase
  setup do
    @comprobante = comprobantes(:one)
  end

  test "visiting the index" do
    visit comprobantes_url
    assert_selector "h1", text: "Comprobantes"
  end

  test "creating a Comprobante" do
    visit comprobantes_url
    click_on "New Comprobante"

    fill_in "Descripcion", with: @comprobante.descripcion
    fill_in "Fecha", with: @comprobante.fecha
    fill_in "Observaciones", with: @comprobante.observaciones
    fill_in "Vacuna", with: @comprobante.vacuna
    click_on "Create Comprobante"

    assert_text "Comprobante was successfully created"
    click_on "Back"
  end

  test "updating a Comprobante" do
    visit comprobantes_url
    click_on "Edit", match: :first

    fill_in "Descripcion", with: @comprobante.descripcion
    fill_in "Fecha", with: @comprobante.fecha
    fill_in "Observaciones", with: @comprobante.observaciones
    fill_in "Vacuna", with: @comprobante.vacuna
    click_on "Update Comprobante"

    assert_text "Comprobante was successfully updated"
    click_on "Back"
  end

  test "destroying a Comprobante" do
    visit comprobantes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Comprobante was successfully destroyed"
  end
end
