class CreateHealthInfos < ActiveRecord::Migration[6.1]
  def change
    create_table :health_infos do |t|
      t.references :paciente, null: false, foreign_key: true
      t.boolean :risk
      t.date :birth

      t.timestamps
    end
  end
end
