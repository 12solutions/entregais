class CreateComprobantes < ActiveRecord::Migration[6.1]
  def change
    create_table :comprobantes do |t|
      t.integer :vacuna
      t.date :fecha
      t.text :descripcion
      t.text :observaciones

      t.timestamps
    end
  end
end
