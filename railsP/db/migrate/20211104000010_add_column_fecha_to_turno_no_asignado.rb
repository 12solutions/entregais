class AddColumnFechaToTurnoNoAsignado < ActiveRecord::Migration[6.1]
  def change
    add_column :turno_no_asignados, :fecha, :DateTime
  end
end
