class AddColumn < ActiveRecord::Migration[6.1]
  def change
    add_reference :health_records, :user, index: true
  end
end
