class AddUserToPersonalInfo < ActiveRecord::Migration[6.1]
  def change
    add_reference :personal_infos, :user, null: false, foreign_key: true
  end
end
