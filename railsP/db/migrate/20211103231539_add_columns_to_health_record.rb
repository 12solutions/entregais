class AddColumnsToHealthRecord < ActiveRecord::Migration[6.1]
  def change
    add_column :health_records, :nombre, :string
    add_column :health_records, :dni, :string
    add_column :health_records, :apellido, :string
  end
end
