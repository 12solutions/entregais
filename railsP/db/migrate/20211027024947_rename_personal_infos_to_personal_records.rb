class RenamePersonalInfosToPersonalRecords < ActiveRecord::Migration[6.1]
  def change
    rename_table :personal_infos, :personal_records
  end
end
