class RemoveColumnUserIdFromHealthRecord < ActiveRecord::Migration[6.1]
  def change
    remove_column :health_records, :user_id, :integer
  end
end
