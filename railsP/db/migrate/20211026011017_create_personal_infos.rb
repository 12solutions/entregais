class CreatePersonalInfos < ActiveRecord::Migration[6.1]
  def change
    create_table :personal_infos do |t|
      t.string :nombre
      t.string :apellido
      t.string :dni

      t.timestamps
    end
  end
end
