class AddUserToComprobantes < ActiveRecord::Migration[6.1]
  def change
    add_reference :comprobantes, :user, null: false, foreign_key: true
  end
end
