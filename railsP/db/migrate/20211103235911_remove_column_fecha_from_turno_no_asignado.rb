class RemoveColumnFechaFromTurnoNoAsignado < ActiveRecord::Migration[6.1]
  def change
    remove_column :turno_no_asignados, :fecha, :string
  end
end
