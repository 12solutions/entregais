class AddColumnToComprobante < ActiveRecord::Migration[6.1]
  def change
    add_column :comprobantes, :printable, :boolean
  end
end
