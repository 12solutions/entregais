class RemoveUserFromHealthRecord < ActiveRecord::Migration[6.1]
  def change
    remove_column :health_records, :references, :user
  end
end
