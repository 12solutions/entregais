class RenameHealthInfosToHealthRecords < ActiveRecord::Migration[6.1]
  def change
    rename_table :health_infos, :health_records
  end
end
