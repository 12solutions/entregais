class CreateTurnoAsignados < ActiveRecord::Migration[6.1]
  def change
    create_table :turno_asignados do |t|
      t.string :lugar
      t.datetime :fecha
      t.string :vacuna # F = fiebre amarillla  ///   G = gripe

      t.timestamps
    end
  end
end
