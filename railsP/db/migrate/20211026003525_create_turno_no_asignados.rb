class CreateTurnoNoAsignados < ActiveRecord::Migration[6.1]
  def change
    create_table :turno_no_asignados do |t|
      t.string :lugar
      t.string :fecha
      t.string :vacuna # F = fiebre amarillla  ///   G = gripe
      t.timestamps
    end
  end
end
