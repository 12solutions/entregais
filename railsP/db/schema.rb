# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_11_04_000010) do

  create_table "comprobantes", force: :cascade do |t|
    t.integer "vacuna"
    t.date "fecha"
    t.text "descripcion"
    t.text "observaciones"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", null: false
    t.boolean "printable"
    t.index ["user_id"], name: "index_comprobantes_on_user_id"
  end

  create_table "health_records", force: :cascade do |t|
    t.boolean "risk"
    t.date "birth"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", null: false
    t.string "nombre"
    t.string "dni"
    t.string "apellido"
    t.index ["user_id"], name: "index_health_records_on_user_id"
  end

  create_table "personal_records", force: :cascade do |t|
    t.string "nombre"
    t.string "apellido"
    t.string "dni"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", null: false
    t.index ["user_id"], name: "index_personal_records_on_user_id"
  end

  create_table "turno_asignados", force: :cascade do |t|
    t.string "lugar"
    t.datetime "fecha"
    t.string "vacuna"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", null: false
    t.index ["user_id"], name: "index_turno_asignados_on_user_id"
  end

  create_table "turno_no_asignados", force: :cascade do |t|
    t.string "lugar"
    t.string "vacuna"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", null: false
    t.datetime "fecha"
    t.index ["user_id"], name: "index_turno_no_asignados_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "role"
    t.string "token"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "comprobantes", "users"
  add_foreign_key "health_records", "users"
  add_foreign_key "personal_records", "users"
  add_foreign_key "turno_asignados", "users"
  add_foreign_key "turno_no_asignados", "users"
end
