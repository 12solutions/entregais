user = User.new
user.email = 'paciente1@example.com'
user.password ='password'
user.password_confirmation ='password'
user.role = "Paciente"
user.token = 1234
user.save!

#turno de primer dosis
t = TurnoAsignado.new
t.user_id = 1
t.vacuna = "Covid1"
t.lugar = "Cementerio"
t.fecha = DateTime.civil(2021,11,12,3,3,3)
t.save!

h = HealthRecord.new
h.nombre ="paciente"
h.apellido ="uno"
h.dni=1
h.birth = DateTime.civil(2019,10,10,2,2,2)
h.user_id = 1
h.save!
#-----------------------------------------------------------------------------------
# PACIENTE 2
#ES MAYOR DE 60 AÑOS
#TIENE UNA DE GRIPE HACE MENOS DE 9 meses 
    #en esta cuenta se va a probar solicitar una segunda dosis de covid y la vacuna de la fiebre amarilla
user = User.new
user.email = 'paciente2@example.com'
user.password ='password'
user.password_confirmation ='password'
user.role = "Paciente"
user.token = 1234
user.save!


#turno de primer dosis
t = TurnoNoAsignado.new
t.user_id = 2
t.vacuna = "Covid1"
t.lugar = "Cementerio"
t.fecha = DateTime.civil(2000,2,2,3,3,3)
t.save!


#gripe
c = Comprobante.new
c.user_id = 2
c.vacuna = 2
c.fecha = DateTime.civil(2021,10,10,2,2,2)
c.save!

h = HealthRecord.new
h.nombre="paciente"
h.apellido="dos"
h.dni=2
h.birth = DateTime.civil(1950,10,10,2,2,2)
h.user_id = 2
h.save!


#-----------------------------------------------------------------------------------
# PACIENTE 3
#YA TIENE VACUNA DE LA FIEBRE AMARILLA
#ES MENOR DE 60 AÑOS
#TIENE UNA DE GRIPE HACE MENOS DE 6 meses 

user = User.new
user.email = 'paciente3@example.com'
user.password ='password'
user.password_confirmation ='password'
user.role = "Paciente"
user.token = 1234
user.save!


#turno de primer dosis
t = TurnoNoAsignado.new
t.user_id = 3
t.vacuna = "Covid1"
t.lugar = "Cementerio"
t.fecha = DateTime.civil(2000,2,2,3,3,3)
t.save!

#fiebre
c = Comprobante.new
c.user_id = 3
c.vacuna = 3
c.descripcion="La vacuna de la fiebre amarilla"
c.observaciones=""
c.printable=true
c.fecha = DateTime.civil(2021,5,10,2,2,2)
c.save!

#gripe
c = Comprobante.new
c.user_id = 3
c.descripcion="La vacuna de la gripe"
c.observaciones="Presento dolor del brazo"
c.vacuna = 2
c.printable=true
c.fecha = DateTime.civil(2021,7,10,2,2,2)
c.save!

h = HealthRecord.new
h.nombre="paciente"
h.apellido="tres"
h.dni=3
h.birth = DateTime.civil(2000,10,10,2,2,2)
h.user_id = 3
h.save!

#-----------------------------------------------------------------------------------


